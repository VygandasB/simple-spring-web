package group.springwebsite.services;

import group.springwebsite.dao.PostsDAO;
import group.springwebsite.entities.Articles;
import group.springwebsite.repositories.PostsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostsService implements PostsDAO {

    @Autowired
    private PostsRepository postsRepository;

    @Override
    public List<Articles> findAll(){
        List<Articles> posts = (List<Articles>) postsRepository.findAll();
        return posts;
    }

    @Override
    public List<Articles> findByName(String name){
        List<Articles> post = postsRepository.findByName(name);
        return post;
    }
}
