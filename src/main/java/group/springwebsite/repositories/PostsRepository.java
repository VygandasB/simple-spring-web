package group.springwebsite.repositories;


import group.springwebsite.entities.Articles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostsRepository extends CrudRepository<Articles, Long> {
    List<Articles> findByName(String name);
}
