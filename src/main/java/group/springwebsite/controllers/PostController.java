package group.springwebsite.controllers;

import group.springwebsite.dao.PostsDAO;
import group.springwebsite.entities.Articles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class PostController {

    @Autowired
    PostsDAO postsDAO;

    @GetMapping(path = "/post/{name}")
    public String findByName(@PathVariable("name") String name, Model model){
        List<Articles> post = postsDAO.findByName(name);
        model.addAttribute("post",post);
        return "post";
    }
}
