package group.springwebsite.dao;

import group.springwebsite.entities.Articles;

import java.util.List;

public interface PostsDAO {
    public List<Articles> findAll();
    public List<Articles> findByName(String name);
}