package group.springwebsite.entities;

import javax.persistence.*;

@Entity
public class Articles {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private String name;
    private String text;
    private String img;

    public Articles(Long id, String name, String text, String img){
        this.id = id;
        this.name = name;
        this.text = text;
        this.img = img;
    }

    public Articles(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
