-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.21-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for website
CREATE DATABASE IF NOT EXISTS `website` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `website`;

-- Dumping structure for table website.articles
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `text` text COLLATE utf8_lithuanian_ci,
  `img` text COLLATE utf8_lithuanian_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- Dumping data for table website.articles: ~2 rows (approximately)
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` (`id`, `name`, `text`, `img`) VALUES
	(1, 'Google Maps is now available in 39 new languages', 'Google Maps is now available in 39 new languages, the company announced today in a blog post. These languages — including Afrikaans, Danish, Filipino, Hebrew, Icelandic, Mongolian, Serbian, Slovak, Swahili, Turkish, and Vietnamese, to name a few — are spoken by around 1.25 billion people.\r\n\r\nKeep in mind there are a total of 6,909 living languages recorded in the Ethnologue catalogue. Many of the languages Google chose to add today are spoken by large populations. Swahili, in particular, has 8 percent of the African continent speaking it, while Turkish is spoken by 9 percent of people in Europe.\r\n\r\nThe new language additions are coming to the mobile and desktop versions of Google Maps on iOS, Android, Mac, and Windows OS. Google Maps initially started off in English in 2004, and has gradually rolled out different language availability over time.\r\n', 'https://cdn.vox-cdn.com/thumbor/P-AofpkXtIHW75CF0lg3wcLr4Q8=/0x0:2040x1360/920x613/filters:focal(857x517:1183x843)/cdn.vox-cdn.com/uploads/chorus_image/image/59177771/DSCF2501__1_.0.jpg'),
	(2, 'Jaguar Land Rover lands £1.2bn deal to supply self-driving cars to Google\'s Waymo ', '\r\n\r\nThe concept of self-driving cars was dealt a blow earlier this month after what is thought to be the first fatality involving a self-driving car.\r\n\r\nPedestrian Elaine Herzberg was hit by an Uber autonomous car on a street at night time in the city of Tempe, Arizona.\r\n\r\nQuestions have been raised about how safe driverless cars on public roads are and on Monday the governor of Arizona put the brakes on Uber\'s self-driving car programme in the state, citing "disturbing and alarming" dashcam footage from the fatal accident.\r\n\r\nJaguar’s electric I-Paces - production versions of which were only officially revealed at the Geneva motor show last month - will go into testing with Waymo later this year.\r\n\r\nJLR said the cars would be fitted with “production ready” self-driving technology. \r\n\r\nThe vehicles cost £60,000 for the cheapest normal model and are the first premium cars to enter Waymo’s fleet. They are expected to start arriving in volume from 2020, with up to 20,000 taking part in development work. On paper the deal could be worth £1.2bn for Jaguar though for such a major arrangement and technology transfer between the two businesses the actual figure is likely to much lower.\r\n\r\nInitial tests in the US - the companies gave no details about when or even if they might appear on UK roads - will help the two businesses develop safety standards and understand what consumers want from self-driving cars.\r\n', 'https://www.telegraph.co.uk/content/dam/business/2016/03/02/ralf-speth_trans_NvBQzQNjv4BqgsaO8O78rhmZrDxTlQBjdEbgHFEZVI1Pljic_pW9c90.jpg?imwidth=1240'),
	(3, 'Arm chips with Nvidia AI could change the Internet of Things', 'Nvidia and Arm today announced a partnership that’s aimed at making it easier for chip makers to incorporate deep learning capabilities into next-generation consumer gadgets, mobile devices and Internet of Things objects. Mostly, thanks to this partnership, artificial intelligence could be coming to doorbell cams or smart speakers soon.\r\n\r\nArm intends to integrate Nvidia’s open-source Deep Learning Accelerator (NVDLA) architecture into its just-announced Project Trillium platform. Nvidia says this should help IoT chip makers incorporate AI into their products.\r\n\r\n“Accelerating AI at the edge is critical in enabling Arm’s vision of connecting a trillion IoT devices,” said Rene Haas, EVP, and president of the IP Group, at Arm. “Today we are one step closer to that vision by incorporating NVDLA into the Arm Project Trillium platform, as our entire ecosystem will immediately benefit from the expertise and capabilities our two companies bring in AI and IoT.”\r\n\r\nAnnounced last month, Arm’s Project Trillium is a series of scalable processors designed for machine learning and neural networks. NVDLA open-source nature allows Arm to offer a suite of developers tools on its new platform. Together, with Arm’s scalable chip platforms and Nvidia’s developer’s tools, the two companies feel they’re offering a solution that could result in billions of IoT, mobile and consumers electronic devices gaining access to deep learning.\r\n\r\nDeepu Tallam, VP and GM of Autonomous Machines at Nvidia, explained it best with this analogy: “NVDLA is like providing all the ingredients for somebody to make it a dish including the instructions. With Arm [this partnership] is basically like a microwave dish.”', 'https://techcrunch.com/wp-content/uploads/2018/03/arm-nvidia.jpg?w=1390&crop=1'),
	(4, 'Huawei P20, P20 Pro with iPhone X-like notch, Kirin 970 SoC launched: Price, specifications', 'Huawei P20 and P20 Pro have been launched at a special event held in Paris. The flagship smartphones include professional grade cameras, notched displays, and updated designs. Huawei is also releasing a Porsche Design Mate RS, featuring an in-display fingerprint scanner and up to 512GB storage. The P20 has already gone on sale for Eur 649 (or approx Rs 52,236)  with 4GB RAM and 128GB storage. The P20 Pro will cost Eur 899 (or approx Rs 72,358)  with 6GB RAM and 128GB storage. Meanwhile, Porsche Design Mate RS will set you back by Eur 2,095 (or approx Rs 1,68,621).\r\n\r\nThe P20 has a 5.8-inch FHD+ LCD display, while the P20 Pro offers a 6.1-inch FHD+ OLED display. Both smartphones get a notch at the top of the display, similar to the iPhone X. They both have a fingerprint scanner on the front but no headphone jack. The P20 and P20 Pro are available in Black, Midnight Blue, Pink Gold, and Twilight. The devices are powered by Huawei’s Kirin 970 processor, up to 6GB RAM, and 128GB storage. The P20 Pro is IP67 water and dust resistant, while the P20 is IP53 certified. The P20 has a 3400mAh battery, while the P20 Pro accommodates a 4000mAh cell. Both smartphones will ship with Huawei’s EMUI 8.1, which is based on Android 8.1 Oreo.\r\n\r\nHuawei says both the P20 and P20 Pro are meant for professional photography. The P20 has an improved dual-camera setup on the back. There’s a 12MP RGB sensor with f/1.8 aperture,  and a 20MP monochrome lens with a f/1.6 aperture. The RGB sensor has 1.55um pixels, each larger than those of the iPhone X. Interestingly, Huawei P20 Pro has a Leica-branded triple lens camera system. Its RGB sensor is a 40MP unit and f/1.6, while its monochrome sensor is 20MP and f/1.6. But the P20 Pro has a third 8MP telephoto lens with 3x optical zoom or a 5x digital zoom. Though, only the telephoto lens has got an OIS (Optical Image Stabilization) feature built-in. The devices have a super slow motion capture feature as well.\r\n\r\nBoth the P20 and P20 Pro should be seen competing with Apple’s iPhone X and Samsung’s Galaxy S9. Over the past few years, Huawei has emerged as the fastest growing smartphone brand. It is currently the world’s third-largest smartphone manufacturer, ahead of Xiaomi and Oppo.', 'http://images.indianexpress.com/2018/03/huawei-p20-7591.jpg'),
	(5, 'Amazon delivers boost to French grocery giant Monoprix ', 'PARIS: Shopping for high-end groceries in France may be about to get a little easier. \r\n\r\nMonoprix, one of France’s top supermarket and household item chains, has announced it will start selling some of its products to Amazon Prime customers in Paris later this year. \r\n\r\nIt is a first for France, where online grocery sales lag far behind the United States and Britain. \r\n\r\n“Amazon and Monoprix announce today a commercial partnership that aims to make Monoprix’s groceries available to Amazon Prime Now customers in Paris and its suburbs this year,” the companies said in a statement on Monday. \r\n\r\nAmazon’s Prime Now service delivers household items to its customers within two hours of an order. \r\n\r\nThe announcement saw the share price of Casino Guichard Perrachon SA, which owns Monoprix, jump 4.05% in Tuesday trading to €39.08 (RM188.63) on the Paris CAC stock market. \r\n\r\nIt also appears set to shake up the retail market in France, which has been hard hit by the growth of online and organic grocery sales. \r\n\r\nLeclerc, another main French supermarket chain, on March 26 also announced a home delivery service in Paris, a move that its head Michel-Edouard Leclerc described as a response to the Monoprix-Amazon alliance.\r\n\r\nFrench media reported widely on Monoprix’s deal with Amazon. \r\n\r\nNewspaper Le Figaro described the alliance as “historic”, while Les Echos on Tuesday ran the headline: “With Monoprix, Casino surrenders to the sirens of Amazon.” — AFP', 'https://www.thestar.com.my/~/media/online/2018/03/27/11/33/dcx_doc6zg8j803wqr1elg1q85k.ashx/?w=620&h=413&crop=1&hash=0F23A95F96D1B19C9B46BB196CAA55C221E171E0'),
	(6, 'The technology behind Amazon\'s surveillance-heavy Go store', 'Despite its online dominance, Amazon isn\'t a stranger to bricks-and-mortar shops. Back in 2015 the firm opened a physical bookshop in Seattle and it\'s promised to open a further 400 more in the next few years.', 'https://wi-images.condecdn.net/image/pjdLzAnoPWy/crop/810/f/GettyImages-696676192.jpg'),
	(7, 'Google is hardwiring Assistant directly into other Androids', 'Google Assistant, the search giant\'s answer to Amazon\'s artificial intelligence assistant Alexa, debuted in May 2016. At the time of its introduction the helper only lived inside the firm\'s messaging app Allo.\r\n\r\nSince then it\'s evolved to occupy Google\'s smart speakers and car products as well as on Android phones. Initially, the AI-assistant was locked inside the Pixel phones and then was adopted by other manufacturers.\r\n\r\nBut Android devices running Google\'s assistant are largely reliant on being able to remotely process the data required to provide answers to queries. This process is done in the cloud.\r\n\r\nNow Google is announcing that it will be embedding the Assistant into the handsets of other manufacturers. "Many of the original equipment manufacturers have announced, or will be announcing, AI chips in their phones," Nick Fox, the vice president of Google Assistant explains. ', 'https://wi-images.condecdn.net/image/j3JpBKyJGKy/crop/810/f/untitled-10.jpg'),
	(8, 'Chrome now blocks dodgy ads by default. Which is great for Google', 'Online advertising is broken. Pop-ups cover half your screen and won\'t go away and autoplay videos blare marketing jingles, driving us to install ad blockers, cutting into publishers\' revenue. With no solution in sight, Google is taking matters into its own hands, launching a filter in the Chrome browser to punish sites for excessive use of the most irritating ads.', 'https://wi-images.condecdn.net/image/5avBpjzYO3l/crop/810/f/19.jpg'),
	(9, 'Twitter’s first ever quarterly profit was driven by a pivot to video', 'The good news: Twitter has made the first quarterly profit in its 12-year history. The bad news: it haemorrhaged users. The good news on the bad news? Most of those users were probably fake.', 'https://wi-images.condecdn.net/image/j2oMkLq1g4B/crop/810/f/GettyImages-187249998.jpg'),
	(10, 'Surface Book 2 review: Microsoft’s stunning Macbook Pro-killer', 'The question has been playing on my mind lately, since before I received a review unit of Microsoft’s new Surface Book 2. I’m writing this on a train on a Bluetooth keyboard connected to my iPad; statistically, you’re most likely reading it on a phone. Recent leaps in power-efficient processors means either of those is perfectly capable enough to tackle most computing tasks.  Full credit to Microsoft: they have been making this argument in recent years with the Surface line – that the old terminology is outdated. The Surface line – from the entry-level Pro to the stunning and stunningly-expensive Surface Studio – are all part of one family; a shared language. Most ordinary users don’t need the power of a Surface Book 2 and will be happy with a Surface Pro. But Microsoft knows that many people need a workhorse computer, one that can handle anything they throw at it - including creative pen inputs, video editing, external monitors – but can be slung in a bag at the end of the day. ', 'https://wi-images.condecdn.net/image/jVrjrvNaNng/crop/810/f/7.jpg');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

-- Dumping structure for table website.articles_authors
CREATE TABLE IF NOT EXISTS `articles_authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- Dumping data for table website.articles_authors: ~0 rows (approximately)
/*!40000 ALTER TABLE `articles_authors` DISABLE KEYS */;
/*!40000 ALTER TABLE `articles_authors` ENABLE KEYS */;

-- Dumping structure for table website.authors
CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `lastname` varchar(45) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- Dumping data for table website.authors: ~0 rows (approximately)
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` (`id`, `firstname`, `lastname`) VALUES
	(1, 'Lukas', 'Budavicius'),
	(2, 'KDAD', 'ASDA');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;

-- Dumping structure for table website.hibernate_sequence
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table website.hibernate_sequence: 1 rows
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` (`next_val`) VALUES
	(1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;

-- Dumping structure for table website.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `body` longtext NOT NULL,
  `date` datetime NOT NULL,
  `title` varchar(300) NOT NULL,
  `author_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table website.posts: ~0 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
